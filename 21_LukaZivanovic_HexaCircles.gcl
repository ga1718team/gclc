%% inits %%
number width  120
number height 120
background 15 15 15

number PI 3.14159265359
number r_init 3
number n 5

dim width height

expression midX {width/2}
expression midY {height/2}
point O midX midY

%% moving point %%
point moving_init -3 -3 6.28318530718 6.28318530718
getx moving_num moving_init

%% moving_parameters %%
if_then_else {moving_num<0}
  {
    expression spread {(moving_num+3)/3}
    expression pulsing_num {0}
    expression clr {100}
    expression rotation {0}
  }
  {
    expression spread {1}
    expression pulsing_num {sin(moving_num)}
    % abs(pulsing_num)
    if_then_else {pulsing_num<0}
      { expression a_pulsing_num {-pulsing_num*2}}
      { expression a_pulsing_num {pulsing_num}}
    expression clr {(a_pulsing_num*50)+100}
    expression rotation {moving_num*(cos(moving_num/2))/2}
  }
  expression clr_in {clr* 0.5}
  expression thc {clr*clr*0.01}

%% outer circle %%
color clr clr clr
linethickness 2
point A 15 midY
circle outer O A
drawcircle outer
color clr_in clr_in clr_in
fillcircle outer

%% other circles %%
linethickness 1
number k 0

% number of "hexagons"
while {k<n}
{
  expression r {r_init*(2*k+2)}
  number i 0
  % hexagon vertexes
  while {i < 6}
  {
    if_then_else {i==0}{color 148 0 211}{}
    if_then_else {i==1}{color 0 0 255}{}
    if_then_else {i==2}{color 0 255 0}{}
    if_then_else {i==3}{color 255 255 0}{}
    if_then_else {i==4}{color 255 127 0}{}
    if_then_else {i==5}{color 255 0 0}{}

    expression x1 {r * cos(i*PI/3*spread + PI/6 + rotation) + midX}
    expression y1 {r * sin(i*PI/3*spread + PI/6 + rotation) + midY}
    point start1 x1 y1

    expression x1_n {pulsing_num * r * cos((i+1)*PI/3*spread + PI/6 + rotation) + midX}
    expression y1_n {pulsing_num * r * sin((i+1)*PI/3*spread*spread + PI/6 + rotation) + midY}
    point end1 x1_n y1_n

    expression tmp {1/(k+1)/2}
    towards start2 start1 end1 tmp

    expression tmp {1 + 1/(k+1)/2}
    towards end2 start1 end1 tmp

    %% circles on the edge of hexagon %%
    number j 0
    while {j <= k}
    {
      expression twd {j/(k+1)}
      towards small_center start1 end1 twd
      towards A2 start2 end2 twd

      circle c1 small_center A2
      drawcircle c1

      if_then_else {j != 0 && pulsing_num > 0}{fillcircle c1}{}

      expression j {j+1}
    }
    expression i {i+1}
  }
  expression k {k+1}
}

animation_frames 120 20
