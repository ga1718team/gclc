# Geometrijski algoritmi @ MATF
# GCLC 

## Zvanična stranica
* http://poincare.matf.bg.ac.rs/~janicic//gclc/

## Uputstvo za studente
* Ako nemate nikakve dodatne fajlove, u root folder (ako ga niste preimenovali, zove se gclc) smestite svoj dokument i nazovite ga rbr\_ImePrezime\_NazivTeme.gcl
* Ako imate neke dodatne falove, u okviru root foldera napraviti svoj folder, čije ime treba da bude u formatu rbr_ImePrezime, unutar tog foldera okačiti gcl fajl čije je ime u formatu ImeTeme.gcl i sve dodatne fajlove.

* Na primer, ako je Vaš seminarski pod rednim brojem 17 u zajedničkoj tabeli, Vi ste Pera Perić i tema Vam se zove Rotirajući krugovi, onda bi fajl trebalo da se zove 17\_PeraPeric\_RotirajuciKrugovi.gcl

* Da biste mogli bilo šta da okačite na server, potrebno je da upišete svoje korisničko ime u našu zajedničku tabelu, kako bih Vas ubacila u grupu korisnika koja ima odgovarajuće privilegije.

## Rokovi
* Rok za prijavu teme je 31.10. 
* Rok za kačenje rešenja je 25.11.
* Odbrana je 28.11. na času vežbi
